#!/usr/bin/env python

# Extracting Diffs from Gitlab with Python
# https://docs.gitlab.com/ee/api/api_resources.html#project-resources

# TODO
#  git-diff extract from gitlab

import requests

# Get all commits
response = requests.get("https://gitlab.com/api/v4/projects/21548885/repository/commits")

commits = response.json()
last_commit_id = commits[0]['id']

# Request for git difference
git_diff_req = requests.get(f"https://gitlab.com/api/v4/projects/21548885/repository/commits/{last_commit_id}/diff")
git_diff_req_res = git_diff_req.json()

# changed router
changed_router = []
router_diff = []
for router in git_diff_req_res:
    if router['new_path'] == 'git-diff.py':
        continue
    else:
        git_dff_path = router['new_path']
        git_diff_folder = git_dff_path.split("/")[0]
        changed_router.append(git_diff_folder)
        if changed_router:
            router_diff.append(router['diff'])


print(changed_router)
print(router_diff)

# Last commit details
last_commit_req = requests.get(f"https://gitlab.com/api/v4/projects/21548885/repository/commits/{last_commit_id}")
last_commit_res = last_commit_req.json()

commits_list = []

for commit in last_commit_res:
    commits_list.append(commit)


print(commits_list)




